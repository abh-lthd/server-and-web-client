var fs = require('fs');
var path = require('path');
var express = require('express');
var bodyParser = require('body-parser');
var cors = require('cors');
var jwt = require('jsonwebtoken');
var multer = require('multer');
var FB = require('fb');
var request = require('request');
var mongo = require('./data/mongo');
var config = require('./config');
var auth = require('./account/authentication');
var authMiddleware = require('./account/auth-middleware');
var upload = require('./photos/upload');
var photoItem = require('./photos/item');
var photoList = require('./photos/list');
var comment = require('./comments/comment');
var commentList = require('./comments/list');
var likePhoto = require('./likes/like');
var unlikePhoto = require('./likes/unlike');
var getLikesOfPhoto = require('./likes/get-likes');
var followUser = require('./following/follow');
var unfollowUser = require('./following/unfollow');
var getFollowsOfUser = require('./following/get-follows');
var safeObjectId = require('./data/safe-object-id');

var app = express();
app.set('port', process.env.PORT || 3000);
app.set('view engine', 'pug');
app.set('views', path.join(__dirname, 'views'));
app.use(express.static('content'));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true, parameterLimit:50000 }));
app.use(bodyParser.json({ limit: '50mb' }));
app.use(cors());

var server = null;

/*---------------------
         PUBLIC ROUTES
---------------------*/
app.post('/api/authentication/login', auth.login);
app.post('/api/authentication/register', auth.register);
app.post('/api/authentication/fb', function(req, res) {
  var accessToken = req.body.accessToken;
  var userId = req.body.userId;

  var clientId = '1798647747090244';
  var clientSecret = '479cda046055940627da4bddcdca9aa0';
  request.get('https://graph.facebook.com/oauth/access_token?client_id=' + clientId + '&client_secret=' + clientSecret + '&grant_type=client_credentials', function(err, response, body) {
    var access_token = body;
    request.get('https://graph.facebook.com/debug_token?input_token=' + accessToken + '&' + access_token, function(err, response, body) {
      var result = JSON.parse(body).data;
      if (!result.is_valid) {
        return res.sendStatus(400);
      }

      // Get use information from facebook
      request.get('https://graph.facebook.com/me?fields=email,name,about&access_token=' + accessToken, function(err, response, body) {
        var userInfo = JSON.parse(body);
        // Check if user already registered with fb before
        var collection = mongo.getDb().collection('users');
        collection.findOne({ email: userInfo.email }).then(function(doc) {
          if (doc) {
            // Already signed in before, generate auth token and send it back to user
            var period = '24h';
            var token = jwt.sign(doc, config.secret, { expiresIn: period });
            res.status(200).json({
              token: token,
              user: doc
            });
          } else {
            // New user, need to add to db
            var newUser = {
              username: userInfo.name,
              email: userInfo.email,
              password: 'fb',
              avatar: 'member/default-avatar.jpg',
              description: 'Hello, world!'
            }
            collection.insertOne(newUser).then(function(r) {
              console.log(r);
              var user = r.ops[0];
              if (user) {
                var period = '24h';
                var token = jwt.sign(user, config.secret, { expiresIn: period });
                res.status(200).json({
                  token: token,
                  user: user
                });
              } else {
                res.sendStatus(400);
              }
            }).catch(function(err) {
              res.sendStatus(400);
            });
          }
        }).catch(function(err) {
          res.sendStatus(400);
        });
      });
    });
  });
});

app.get('/api/photos/:id/info', photoItem.photoInfo);
app.get('/api/photos/:id', photoItem.displayPhoto);

app.get('/api/comments/:photo_id', commentList.getAllComments);

// Get all likes of a photo
app.get('/api/likes/:photo_id', getLikesOfPhoto);

// Get all follows of a user
app.get('/api/follows/:target_user_id/followers', getFollowsOfUser);

// Search all users by username
app.get('/api/users', function(req, res) {
  var username = req.query.username;

  var collection = mongo.getDb().collection('users');
  var usernameRegex = new RegExp('.*' + username + '.*');
  collection.find({ username: usernameRegex })
    .toArray()
    .then(function(users) {
      if (users.length) {
        res.json(users);
      } else {
        res.sendStatus(404);
      }
    })
    .catch(function(err) {
      res.sendStatus(400);
    });
});

// Search all users by username
app.get('/api/users/:user_id/info', function(req, res) {
  var userId = req.params.user_id;
  var collection = mongo.getDb().collection('users');
  collection.findOne({ _id: safeObjectId(userId) })
    .then(function(user) {
      if (user) {
        res.send(user);
      } else {
        res.sendStatus(404);
      }
    })
    .catch(function(err) {
      res.sendStatus(400);
    });
});

app.get('/api/users/:user_id/photos', function(req, res) {
  var userId = req.params.user_id;

  var collection = mongo.getDb().collection('users');
  collection.aggregate([
    {
      $lookup: {
        from: 'photos',
        localField: '_id',
        foreignField: 'user_id',
        as: 'photos'
      }
    },
    {
      $match: {
        _id: safeObjectId(userId)
      }
    }
  ]).toArray()
    .then(function(docs) {
      if (docs.length === 0) {
        res.status(404).send('User not found.')
      } else {
        var result = docs[0];
        res.status(200).send(result);
      }
    })
    .catch(function(err) {
      res.sendStatus(400);
    });
});

app.get('/api/follows/:user_id/following', function(req, res) {
  var userId = req.params.user_id;
  var collection = mongo.getDb().collection('follows');
  collection.find({ user_id: safeObjectId(userId) }).toArray().then(function(docs) {
    res.send(docs);
  }).catch(function(err) {
    res.sendStatus(400);
  });
});

/*---------------------
         GUARD
---------------------*/
app.use(authMiddleware);

/*---------------------------
      PROTECTED ROUTES
 --------------------------*/

app.get('/api/users/authed', function(req, res) {
  var user = req.user;
  delete user.password;
  delete user.iat;
  delete user.exp;

  res.json(user);
});

// Change user's password
app.post('/api/users/password', function(req, res) {
  var userId = req.user._id;
  var oldPassword = req.body.old_password;
  var newPassword = req.body.new_password;

  // Check if new password is valid
  var passwordRegex = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d$@$!%*#?&]{6,}$/;
  if (!passwordRegex.test(newPassword)) {
    return res.status(400).send('Password is invalid.')
  }

  var collection = mongo.getDb().collection('users');
  collection.findOneAndUpdate(
  {
    _id: safeObjectId(userId),
    password: oldPassword
  },
  {
    $set: { password: newPassword }
  },
  {
    returnOriginal: false
  }).then(function(r) {
      var updatedUser = r.value;
      if (updatedUser) {
        res.status(200).send(updatedUser)
      } else {
        res.status(404).send('Your password does not match.');
      }
    })
    .catch(function(err) {
      res.sendStatus(400);
    });
});

// Change user's avatar and description
var storage = multer.diskStorage({
  destination: function(req, file, callback) {
    callback(null, './content/member');
  },
  filename: function(req, file, callback) {
    callback(null, file.originalname);
  }
});
app.post('/api/users/info', function(req, res) {
  var userId = req.user._id;
  var collection = mongo.getDb().collection('users');

  var upload = multer({ storage: storage }).single('avatar');
  upload(req, res, function(err) {
    if (err) res.sendStatus(400);

    var description = req.body.description;
    var updateInfo = {
      description: description
    };

    if (req.file) {
      updateInfo.avatar = 'member/' + req.file.filename
    }

    // Update member
    collection.findOneAndUpdate(
    {
      _id: safeObjectId(userId)
    },
    {
      $set: updateInfo
    },
    {
      returnOriginal: false
    }).then(function(r) {
        if (r.value) {
          res.status(200).send(r.value);
        } else {
          res.status(404).send('User not found.');
        }
      })
      .catch(function(err) {
        res.sendStatus(400);
      });
  });

});

app.post('/api/photos/upload', upload);

// Update photo status
app.post('/api/photos/:id/status', photoItem.updateStatus);
// Delete photo
app.delete('/api/photos/:id', photoItem.deletePhoto);
// Get all photos uploaded by current user
app.get('/api/photos', photoList.getMyPhotos);

// Post a comment
app.post('/api/comments', comment.postComment);
// Update a comment
app.put('/api/comments/:comment_id', comment.updateComment);
// Delete a comment
app.delete('/api/comments/:comment_id', comment.deleteComment);

// Like a photo
app.post('/api/likes/:photo_id', likePhoto);
// Unlike a photo
app.delete('/api/likes/:photo_id', unlikePhoto);
// Get <limit> users have the largest followers (those users are not follewed by current user)
app.get('/api/follows/users', function(req, res) {
  var filter = req.query.filter;
  var limit = parseInt(req.query.limit) || 5;
  var userId = req.user._id;

  if (filter !== 'largest') {
    return res.status(400).send('Invalid filter.');
  }

  var collection = mongo.getDb().collection('users');
  collection.aggregate([
    {
      $lookup: {
        from: 'follows',
        localField: '_id',
        foreignField: 'target_user_id',
        as: 'followers'
      }
    },
    {
      $group: {
        _id: {
          _id: '$_id',
          username: '$username',
          email: '$email',
          avatar: '$avatar',
          description: '$description',
          followers: '$followers'
        },
        total_followers: {
          $sum: {
            $size: '$followers'
          }
        }
      }
    },
    {
      $match: {
        '_id.followers.user_id': {
          $ne: safeObjectId(userId)
        }
      }
    },
    {
      $sort: {
        total_followers: -1
      }
    },
    {
      $limit: limit
    },
    {
      $project: {
        _id: '$_id._id',
        username: '$_id.username',
        email: '$_id.email',
        avatar: '$_id.avatar',
        description: '$_id.description'
      }
    },
    {
      $match: {
        '_id': {
          $ne: safeObjectId(userId)
        }
      }
    },
  ]).toArray()
    .then(function(docs) {
      if (docs.length > 0) {
        return res.status(200).json(docs);
      } else {
        return res.sendStatus(404);
      }
    }).catch(function(err) {
      res.sendStatus(400);
    });
});
// Get all photos of users who current user is following
app.get('/api/follows/photos', function(req, res) {
  var userId = req.user._id;

  // Get all users this user is following
  var usersCol = mongo.getDb().collection('users');
  usersCol.aggregate([
    {
      $lookup: {
        from: 'follows',
        localField: '_id',
        foreignField: 'target_user_id',
        as: 'followers'
      }
    },
    {
      $match: {
        'followers.user_id': safeObjectId(userId)
      }
    },
    {
      $project: {
        _id: '$_id',
        username: '$username',
        email: '$email',
        avatar: '$avatar',
        description: '$description'
      }
    },
    {
      $out: 'tmpTargetUsers'
    }
  ]).toArray()
    .then(function(docs) {
      var photosCol = mongo.getDb().collection('photos');
      photosCol.aggregate([
        {
          $lookup: {
            from: 'tmpTargetUsers',
            localField: 'user_id',
            foreignField: '_id',
            as: 'uploader'
          }
        },
        {
          $sort: {
            date: -1
          }
        },
        {
          $project: {
            _id: '$_id',
            filename: '$filename',
            destination: '$destination',
            user_id: '$user_id',
            status: '$status',
            date: '$date',
            uploader: {
              $arrayElemAt: ['$uploader', 0]
            }
          }
        }
      ]).toArray()
        .then(function(photos) {
          if (photos.length > 0) {
            return res.status(200).json(photos);
          } else {
            return res.sendStatus(404);
          }
        }).catch(function(err) {
          res.sendStatus(400)
        });
    })
    .catch(function(err) {
      res.sendStatus(400)
    });
});
// Follow a user
app.post('/api/follows/:target_user_id', followUser);
app.delete('/api/follows/:target_user_id', unfollowUser);

app.start = function() {
  return mongo.connect().then(function() {
    server = app.listen(app.get('port'), function(err) {
      if (err) throw err;

      console.log('Server is listening on port', app.get('port'));
    });
  }).catch(function(err) {
    throw err;
  });
};

app.stop = function() {
  server.close();
};

app.stopAndClean = function() {
  mongo.dropDatabase();
  server.close();
}

module.exports = app;
