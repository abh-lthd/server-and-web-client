var mongo = require('../data/mongo');
var safeObjectId = require('../data/safe-object-id');
var status = require('../http/status');

module.exports = function(req, res) {
  var photoId = req.params.photo_id;
  var userId = req.user._id;

  var likeCollection = mongo.getDb().collection('likes');
  var photoCollection = mongo.getDb().collection('photos');

  photoCollection.findOne({ _id: safeObjectId(photoId) })
    .then(function(photo) {
      if (!photo) {
        return res.status(404).send('Photo with id ' + photoId + ' does not exists.');
      }

      // Insert like
      likeCollection.insertOne({
        photo_id: safeObjectId(photoId),
        user_id: safeObjectId(userId)
      }).then(function(r) {
        if (r.insertedCount === 1) {
          var like = r.ops[0];
          res.status(200).json(like);
        } else {
          res.sendStatus(400);
        }
      }).catch(function(err) {
        res.sendStatus(400);
      });
    }).catch(function(err) {
      res.status(400);
    });
};
