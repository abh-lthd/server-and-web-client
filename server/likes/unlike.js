var mongo = require('../data/mongo');
var safeObjectId = require('../data/safe-object-id');
var status = require('../http/status');

module.exports = function(req, res) {
  var photoId = req.params.photo_id;
  var userId = req.user._id;

  var collection = mongo.getDb().collection('likes');

  collection.findOneAndDelete({
    photo_id: safeObjectId(photoId),
    user_id: safeObjectId(userId)
  }).then(function(r) {
    var like = r.value;
    if (like) {
      res.status(200).json(like);
    } else {
      res.sendStatus(404);
    }
  }).catch(function(err) {
    res.status(400);
  });
};
