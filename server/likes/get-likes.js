var mongo = require('../data/mongo');
var safeObjectId = require('../data/safe-object-id');
var status = require('../http/status');

module.exports = function(req, res) {
  var photoId = req.params.photo_id;

  var collection = mongo.getDb().collection('likes');
  collection.find({ photo_id: safeObjectId(photoId) })
    .toArray()
    .then(function(likes) {
      if (likes.length === 0) {
        return res.status(404).send('Photo with id ' + photoId + ' not found.');
      }

      var userCollection = mongo.getDb().collection('users');
      var likesWithUser = [];
      likes.forEach(function(like) {
        userCollection.findOne({ _id: safeObjectId(like.user_id) })
          .then(function(user) {
            delete like.user_id;
            if (user) {
              delete user.password;
              like.user = user;
            } else {
              like.user = null;
            }
            likesWithUser.push(like);
          }).catch(function(err) {
            res.sendStatus(400);
          });
      });
      var loop = setInterval(function() {
        if (likesWithUser.length === likes.length) {
          res.status(200).json(likesWithUser);
          clearInterval(loop);
        }
      }, 0);
    }).catch(function(err) {
      res.sendStatus(400);
    });
};
