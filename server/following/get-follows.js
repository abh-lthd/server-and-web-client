var mongo = require('../data/mongo');
var safeObjectId = require('../data/safe-object-id');
var status = require('../http/status');

module.exports = function(req, res) {
  var targetUserId = req.params.target_user_id;

  var collection = mongo.getDb().collection('follows');
  collection.find({ target_user_id: safeObjectId(targetUserId) })
    .toArray()
    .then(function(follows) {
      if (follows.length === 0) {
        return res.status(404).send('User with id ' + targetUserId + ' not found.');
      }

      var userCollection = mongo.getDb().collection('users');
      var followsWithUser = [];
      follows.forEach(function(follow) {
        userCollection.findOne({ _id: safeObjectId(follow.user_id) })
          .then(function(user) {
            delete follow.user_id;
            if (user) {
              delete user.password;
              follow.user = user;
            } else {
              follow.user = null;
            }
            followsWithUser.push(follow);
          }).catch(function(err) {
            res.sendStatus(400);
          });
      });
      var loop = setInterval(function() {
        if (followsWithUser.length === follows.length) {
          res.status(200).json(followsWithUser);
          clearInterval(loop);
        }
      }, 0);
    }).catch(function(err) {
      res.sendStatus(400);
    });
};
