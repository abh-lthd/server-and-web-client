var mongo = require('../data/mongo');
var safeObjectId = require('../data/safe-object-id');
var status = require('../http/status');

module.exports = function(req, res) {
  var targetUserId = req.params.target_user_id;
  var userId = req.user._id;

  var collection = mongo.getDb().collection('follows');

  collection.findOneAndDelete({
    user_id: safeObjectId(userId),
    target_user_id: safeObjectId(targetUserId)
  }).then(function(r) {
    var follow = r.value;
    if (follow) {
      res.status(200).json(follow);
    } else {
      res.sendStatus(404);
    }
  }).catch(function(err) {
    res.status(400);
  });
};
