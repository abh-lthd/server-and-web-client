var mongo = require('../data/mongo');
var safeObjectId = require('../data/safe-object-id');
var status = require('../http/status');

module.exports = function(req, res) {
  var targetUserId = req.params.target_user_id;
  var userId = req.user._id;

  var followCollection = mongo.getDb().collection('follows');
  var userCollection = mongo.getDb().collection('users');

  userCollection.findOne({ _id: safeObjectId(targetUserId) })
    .then(function(user) {
      if (!user) {
        return res.status(404).send('User with id ' + targetUserId + ' does not exists.');
      }

      // Insert follow
      followCollection.insertOne({
        user_id: safeObjectId(userId),
        target_user_id: safeObjectId(targetUserId)
      }).then(function(r) {
        if (r.insertedCount === 1) {
          var follow = r.ops[0];
          res.status(200).json(follow);
        } else {
          res.sendStatus(400);
        }
      }).catch(function(err) {
        res.sendStatus(400);
      });
    }).catch(function(err) {
      res.status(400);
    });
};
