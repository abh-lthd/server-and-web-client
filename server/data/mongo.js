var MongoClient = require('mongodb').MongoClient;
var config = require('../config');

var dbInstance = null;
exports.connect = function() {
  return new Promise(function(resolve, reject) {
    var dbUri = config[process.env.NODE_ENV || 'development'].dbUri;
    MongoClient.connect(dbUri, function(err, db) {
      if (err) return reject(err);

      dbInstance = db;
      resolve();
    });
  });
};

exports.getDb = function() {
  return dbInstance;
};

exports.close = function() {
  if (dbInstance !== null) {
    dbInstance.close();
  }
};

exports.dropDatabase = function() {
  if (dbInstance != null) {
    dbInstance.dropDatabase();
  }
};
