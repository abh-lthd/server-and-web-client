var ObjectId = require('mongodb').ObjectId;

module.exports = function(s) {
  return ObjectId.isValid(s) ? new ObjectId(s) : null;
};
