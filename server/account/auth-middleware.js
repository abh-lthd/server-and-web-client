var jwt = require('jsonwebtoken');
var config = require('../config');
var status = require('../http/status');

module.exports = function(req, res, next) {
  if (!req.headers.authorization || !req.headers.authorization.startsWith('Bearer ')) {
    // Invalid token
    return res.sendStatus(status.NOT_FOUND);
  }

  var token = req.headers.authorization.split(" ")[1];
  // Verify token
  jwt.verify(token, config.secret, function(err, decoded) {
    if (err) return res.sendStatus(status.NOT_FOUND);

    req.user = decoded;
    next();
  });
};
