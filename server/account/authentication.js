var jwt = require('jsonwebtoken');
var mongo = require('../data/mongo');
var config = require('../config');
var status = require('../http/status');

var userCollectionName = 'users';

function generateAuthToken(user) {
  var period = '24h';
  var token = jwt.sign(user, config.secret, { expiresIn: period });
  return token;
}

function auth(username, password, collection) {
  return collection.findOne({
    username: username,
    password: password
  }).then(function(user) {
    if (!user) {
      throw new Error('User not found.');
    }
    return user;
  }).catch(function(err) {
    throw err;
  });
}

function login(req, res) {
  var username = req.body.username;
  var password = req.body.password;

  var collection = mongo.getDb().collection(userCollectionName);
  auth(username, password, collection).then(function(user) {
    token = generateAuthToken(user);

    delete user.password;
    res.status(status.SUCCESS).json({
      token: token,
      user: user
    });
  }).catch(function(err) {
    res.sendStatus(status.NOT_FOUND);
  });
}

function isCanRegister(username, email, collection) {
  return collection.findOne({
    $or: [
      { username: username },
      { email: email }
    ]
  }).then(function(user) {
    if (user) {
      var error = null;
      if (user.username === username) {
        error = 'Username ' + username + ' already exists.';
      } else if (user.email === email) {
        error = 'Email ' + email + ' already exists.';
      }
      if (error) throw new Error(error);
    }
  }).catch(function(err) {
    throw err;
  });
}

function addUser(user, collection) {
  return collection.insertOne({
    username: user.username,
    email: user.email,
    password: user.password,
    avatar: user.avatar,
    description: user.description
  }).then(function(result) {
    if (result.insertedCount !== 1) {
      throw new Error('Failed to add new user.');
    }
    return result;
  }).catch(function(err) {
    throw err;
  });
}

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function register(req, res) {
  var username = req.body.username;
  var password = req.body.password;
  var email = req.body.email;

  var usernameRegex = /^[a-zA-Z0-9._-]+$/;
  if (!usernameRegex.test(username)) {
    return res.status(status.BAD_REQUEST).send('Username is invalid.')
  }

  if (!validateEmail(email)) {
    return res.status(status.BAD_REQUEST).send('Email is invalid.')
  }

  var passwordRegex = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d$@$!%*#?&]{6,}$/;
  if (!passwordRegex.test(password)) {
    return res.status(status.BAD_REQUEST).send('Password is invalid.')
  }

  var collection = mongo.getDb().collection(userCollectionName);

  isCanRegister(username, email, collection).then(function() {
    var defaultAvatar = 'member/default-avatar.jpg';
    var defaultDescription = 'Hello, world!';
    addUser({
      username: username,
      email: email,
      password: password,
      avatar: defaultAvatar,
      description: defaultDescription
    }, collection).then(function() {
      res.sendStatus(status.SUCCESS);
    }).catch(function(err) {
      res.sendStatus(status.BAD_REQUEST);
    });
  }).catch(function(err) {
    res.status(status.CONFLICT).send(err.message);
  });
}

module.exports = {
  login: login,
  register: register
};
