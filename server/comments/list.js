var mongo = require('../data/mongo');
var safeObjectId = require('../data/safe-object-id');
var status = require('../http/status');

function getAllComments(req, res) {
  var photoId = req.params.photo_id;

  var collection = mongo.getDb().collection('comments');
  collection.find({ photo_id: safeObjectId(photoId) })
    .toArray()
    .then(function(comments) {
      if (comments.length > 0) {
        var commentsWithUser = [];
        // Find user associated with each comment
        var userCollection = mongo.getDb().collection('users');
        comments.forEach(function(comment) {
          userCollection.findOne({ _id: safeObjectId(comment.user_id) })
            .then(function(user) {
              delete comment.user_id;
              delete user.password;
              comment.user = user;
              commentsWithUser.push(comment);
            }).catch(function(err) {
              comment.user = null;
              commentsWithUser.push(comment);
            });
        });
        var loop = setInterval(function() {
          if (commentsWithUser.length === comments.length) {
            res.status(200).json(commentsWithUser);
            clearInterval(loop);
          }
        }, 0);
      } else {
        res.sendStatus(404);
      }
    }).catch(function(err) {
      res.sendStatus(400);
    });
}

module.exports = {
  getAllComments: getAllComments
};
