var mongo = require('../data/mongo');
var safeObjectId = require('../data/safe-object-id');
var status = require('../http/status');

function postComment(req, res) {
  var photoId = req.body.photo_id;
  var userId = req.user._id;
  var comment = req.body.comment;

  if (!comment) {
    return res.status(400).send('Your comment can not be empty.');
  }

  var commentCollection = mongo.getDb().collection('comments');
  var photoCollection = mongo.getDb().collection('photos');

  // Find if photo exists or not
  photoCollection.findOne({ _id: safeObjectId(photoId) }).then(function(photo) {
    if (photo == null) {
      return res.status(404).send('Photo with id ' + photoId + ' does not exists.');
    }

    commentCollection.insertOne({
      user_id: safeObjectId(userId),
      photo_id: safeObjectId(photoId),
      comment: comment
    }).then(function(result) {
      if (result.insertedCount === 1) {
        var addedComment = result.ops[0];
        res.status(200).json(addedComment);
      } else {
        res.sendStatus(400);
      }
    }).catch(function(err) {
      res.sendStatus(400);
    });
  }).catch(function(err) {
    return res.sendStatus(400);
  });
}

function deleteComment(req, res) {
  var userId = req.user._id;
  var commentId = req.params.comment_id;

  // Check if this comment exists is owned by current user
  var collection = mongo.getDb().collection('comments');
  collection.findOne({ _id: safeObjectId(commentId) })
    .then(function(comment) {
      if (comment == null) {
        return res.sendStatus(404);
      }
      var ownId = comment.user_id;
      if (ownId != userId) {
        return res.sendStatus(403);
      }
      // Delete comment
      collection.findOneAndDelete({ _id: safeObjectId(commentId) }).then(function(r) {
        if (r.value._id.toString() === commentId) {
          return res.sendStatus(200);
        } else {
          res.sendStatus(404);
        }
      }).catch(function(err) {
        res.sendStatus(400);
      });
    }).catch(function(err) {
      res.status(400);
    });
}

function updateComment(req, res) {
  var commentId = req.params.comment_id;
  var userId = req.user._id;
  var newComment = req.body.new_comment;

  if (!newComment) {
    return res.sendStatus(400);
  }

  // Check if this comment exists is owned by current user
  var collection = mongo.getDb().collection('comments');
  collection.findOne({ _id: safeObjectId(commentId) })
    .then(function(comment) {
      if (comment == null) {
        return res.sendStatus(404);
      }
      var ownId = comment.user_id;
      if (ownId != userId) {
        return res.sendStatus(403);
      }
      // Update comment
      collection.findOneAndUpdate(
        { _id: safeObjectId(commentId) },
        { $set: { comment: newComment } },
        { returnOriginal: false }
      ).then(function(r) {
        var updatedComment = r.value;
        return res.status(200).json(updatedComment);
      }).catch(function(err) {
        res.sendStatus(404);
      })
    }).catch(function(err) {
      res.status(400);
    });
}

module.exports = {
  postComment: postComment,
  updateComment: updateComment,
  deleteComment: deleteComment
};
