module.exports = {
  development: {
    dbUri: 'mongodb://localhost:27017/photos'
  },
  test: {
    dbUri: 'mongodb://localhost:27017/photos-test'
  },
  production: {
    dbUri: 'mongodb://abh:photosgram@ds155028.mlab.com:55028/photosgram'
  },
  secret: 'photos-app'
};
