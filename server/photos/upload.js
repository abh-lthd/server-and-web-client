var multer = require('multer');
var mongo = require('../data/mongo');
var safeObjectId = require('../data/safe-object-id');

var storage = multer.diskStorage({
  destination: function(req, file, callback) {
    callback(null, './content/upload');
  },
  filename: function(req, file, callback) {
    callback(null, file.fieldname + '-' + Date.now() + '.png');
  }
});

module.exports = function(req, res) {
  var upload = multer({ storage: storage }).single('photo');
  upload(req, res, function(err) {
    if (err) res.sendStatus(400);

    var photoToInsert = {
      filename: req.file.filename,
      destination: req.file.destination,
      user_id: safeObjectId(req.user._id),
      status: req.body.status,
      date: new Date()
    };

    var collection = mongo.getDb().collection('photos');
    // We can get inserted _id through photoToInsert argument
    collection.insertOne(photoToInsert).then(function(result) {
      if (result.insertedCount === 1) {
        return res.status(200).json({ photo_id: photoToInsert._id });
      } else {
        throw new Error('Can not save photo.');
      }
    }).catch(function(err) {
      res.sendStatus(400);
    });
  });
};
