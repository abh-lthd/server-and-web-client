var mongo = require('../data/mongo');
var safeObjectId = require('../data/safe-object-id');

function getMyPhotos(req, res) {
  var collection = mongo.getDb().collection('photos');
  var userId = req.user._id;
  collection.find({ user_id: safeObjectId(userId) }).toArray().then(function(photos) {
    res.json(photos);
  }).catch(function(err) {
    res.sendStatus(404);
  });
}

module.exports = {
  getMyPhotos: getMyPhotos
};
