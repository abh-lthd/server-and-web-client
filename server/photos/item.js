var path = require('path');
var mongo = require('../data/mongo');
var safeObjectId = require('../data/safe-object-id');
var status = require('../http/status');

module.exports = {
  displayPhoto: function(req, res) {
    var photoId = req.params.id;

    var collection = mongo.getDb().collection('photos');
    collection.findOne({ _id: safeObjectId(photoId) }).then(function(photo) {
      if (photo !== null) {
        var relativePath = photo.destination + '/' + photo.filename;
        var absolutePath = path.resolve(__dirname, '..', '..', relativePath);
        return res.status(status.SUCCESS).sendFile(absolutePath);
      } else {
        throw new Error('Can not get photo.');
      }
    }).catch(function(err) {
      res.sendStatus(status.NOT_FOUND);
    });
  },

  photoInfo: function(req, res) {
    var photoId = req.params.id;

    var collection = mongo.getDb().collection('photos');
    collection.findOne({ _id: safeObjectId(photoId) }).then(function(photo) {
      if (photo !== null) {
        return res.status(status.SUCCESS).json(photo);
      } else {
        throw new Error('Can not get photo.');
      }
    }).catch(function(err) {
      res.sendStatus(status.NOT_FOUND);
    });
  },

  deletePhoto: function(req, res) {
    var photoId = req.params.id;
    var collection = mongo.getDb().collection('photos');
    collection.findOneAndDelete({ _id: safeObjectId(photoId) }).then(function(r) {
      if (r.value._id.toString() === photoId) {
        return res.sendStatus(200);
      } else {
        throw new Error('Failed to delete photo.');
      }
    }).catch(function(err) {
      res.sendStatus(404);
    });
  },

  updateStatus: function(req, res) {
    var photoId = req.params.id;
    var newStatus = req.body.new_status;

    var collection = mongo.getDb().collection('photos');
    collection.findOneAndUpdate(
      { _id: safeObjectId(photoId) },
      { $set: { status: newStatus } },
      { returnOriginal: false }
    ).then(function(r) {
      var updatedPhoto = r.value;
      return res.status(200).json(updatedPhoto);
    }).catch(function(err) {
      res.sendStatus(404);
    });
  }
};
