var spawn = require('child_process').spawn;
var exec = require('child_process').exec;

exec('mongod --dbpath ./content/data');

var test = false;
process.argv.forEach(function (val, index, array) {
  if (val === '-t' || val == '--test') {
    spawn('npm.cmd', ['test'], { stdio: 'inherit' });
    test = true;
  }
});

if (test === false) {
  spawn('npm.cmd', ['run', 'server'], { stdio: 'inherit' });
  spawn('npm.cmd', ['run', 'client'], { stdio: 'inherit' });
  exec('sass --watch client/scss:client/css');
}
