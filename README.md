# Hướng dẫn cài đặt:

## Phần mềm yêu cầu:

* NodeJS
* MongoDB version >= 3.2
* Node modules: `npm install`

## Run ##
### Ở thư mục root của project, chạy lần lượt các lệnh:
* `npm install`
* Khởi chạy Database: `mongod --dbpath ./content/data`
* Chạy server: `npm run server`
* Chạy client: `npm run client`
* Test: `node dev -t`