var expect = require('chai').expect;
var fs = require('fs');
var request = require('request');
var app = require('../server/app');

var url = 'http://localhost:3000';
var token = null;
var photoId = null;
var commentId = null;

describe('Comment testing', function() {
  before(function(done) {
    app.start().then(done).catch(done);
  });

  it("should signup successfully", function(done) {
    request({
      url: url + '/api/authentication/register',
      method: 'POST',
      dataType: 'json',
      json: {
        username: 'jinich',
        email: 'jinich@jinich.jinich',
        password: '1234qwer'
      }
    }, function(err, response, body) {
      expect(response.statusCode).to.equal(200);
      done();
    });
  });

  it("should login successfully", function(done) {
    request({
      url: url + '/api/authentication/login',
      method: 'POST',
      dataType: 'json',
      json: {
        username: 'jinich',
        password: '1234qwer'
      }
    }, function(err, response, body) {
      expect(response.statusCode).to.equal(200);
      token = body.token;
      done();
    });
  });

  it("should upload photo to server", function(done) {
    var formData = {
      // Pass data via Streams
      photo: fs.createReadStream(__dirname + '/image.png'),
      status: 'Chuc mung nam moi'
    };
    request({
      url: 'http://localhost:3000/api/photos/upload',
      method: 'POST',
      headers: {
        Authorization: 'Bearer ' + token
      },
      formData: formData
    }, function (err, response, body) {
      expect(response.statusCode).to.equal(200);
      photoId = JSON.parse(body).photo_id;
      done();
    });
  });

  it('should post a comment', function(done) {
    request({
      url: url + '/api/comments/',
      method: 'POST',
      headers: {
        Authorization: 'Bearer ' + token
      },
      dataType: 'json',
      json: {
        photo_id: photoId,
        comment: 'What a nice photo!!!'
      }
    }, function(err, response, body) {
      expect(response.statusCode).to.equal(200);
      expect(body.photo_id).to.equal(photoId);
      commentId = body._id;
      done();
    });
  });

  it('should can not post comment because photo not found.', function(done) {
    request({
      url: url + '/api/comments/',
      method: 'POST',
      headers: {
        Authorization: 'Bearer ' + token
      },
      dataType: 'json',
      json: {
        photo_id: 'invalid photo id',
        comment: 'What a nice photo!!!'
      }
    }, function(err, response, body) {
      expect(response.statusCode).to.equal(404);
      done();
    });
  });

  it('should can not post comment because comment is empty.', function(done) {
    request({
      url: url + '/api/comments/',
      method: 'POST',
      headers: {
        Authorization: 'Bearer ' + token
      },
      dataType: 'json',
      json: {
        photo_id: photoId,
        comment: ''
      }
    }, function(err, response, body) {
      expect(response.statusCode).to.equal(400);
      done();
    });
  });

  it('should get all comments of a photo.', function(done) {
    request(url + '/api/comments/' + photoId, function(err, response, body) {
      expect(response.statusCode).to.equal(200);
      expect(JSON.parse(body).length).to.equal(1);
      done();
    });
  });

  it('should should return not found because photo does not exists.', function(done) {
    request(url + '/api/comments/' + 'invalid photo id', function(err, response, body) {
      expect(response.statusCode).to.equal(404);
      done();
    });
  });

  it('should update comment', function(done) {
    request({
      url: url + '/api/comments/' + commentId,
      method: 'PUT',
      headers: {
        Authorization: 'Bearer ' + token
      },
      dateType: 'json',
      json: {
        new_comment: 'hihi'
      }
    }, function(err, response, body) {
      expect(response.statusCode).to.equal(200);
      expect(body._id).to.equal(commentId);
      expect(body.comment).to.equal('hihi');
      done();
    });
  });

  it('should can not update comment because comment does not exists', function(done) {
    request({
      url: url + '/api/comments/' + 'commentId',
      method: 'PUT',
      headers: {
        Authorization: 'Bearer ' + token
      },
      dateType: 'json',
      json: {
        new_comment: 'hihi'
      }
    }, function(err, response, body) {
      expect(response.statusCode).to.equal(404);
      done();
    });
  });

  it('should can not update comment because new_comment is empty.', function(done) {
    request({
      url: url + '/api/comments/' + 'commentId',
      method: 'PUT',
      headers: {
        Authorization: 'Bearer ' + token
      },
      dateType: 'json',
      json: {
        new_comment: ''
      }
    }, function(err, response, body) {
      expect(response.statusCode).to.equal(400);
      done();
    });
  });

  it('should delete a comment', function(done) {
    request({
      url: url + '/api/comments/' + commentId,
      method: 'DELETE',
      headers: {
        Authorization: 'Bearer ' + token
      }
    }, function(err, response, body) {
      expect(response.statusCode).to.equal(200);
      done();
    });
  });

  it('should can not delete comment becouse comment not exists.', function(done) {
    request({
      url: url + '/api/comments/' + 'invalid comment id',
      method: 'DELETE',
      headers: {
        Authorization: 'Bearer ' + token
      }
    }, function(err, response, body) {
      expect(response.statusCode).to.equal(404);
      done();
    });
  });

  after(function() {
    app.stopAndClean();
  });
});
