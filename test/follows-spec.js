var expect = require('chai').expect;
var fs = require('fs');
var request = require('request');
var app = require('../server/app');

var url = 'http://localhost:3000';
var token = null;
var userId = null;
var targetUserToken = null;
var targetUser = null;
var followId = null;

describe('Follows testing', function() {
  before(function(done) {
    app.start().then(done).catch(done);
  });

  it("should signup successfully", function(done) {
    request({
      url: url + '/api/authentication/register',
      method: 'POST',
      dataType: 'json',
      json: {
        username: 'jinich',
        email: 'jinich@jinich.jinich',
        password: '1234qwer'
      }
    }, function(err, response, body) {
      expect(response.statusCode).to.equal(200);
      done();
    });
  });

  it("should login successfully", function(done) {
    request({
      url: url + '/api/authentication/login',
      method: 'POST',
      dataType: 'json',
      json: {
        username: 'jinich',
        password: '1234qwer'
      }
    }, function(err, response, body) {
      expect(response.statusCode).to.equal(200);
      token = body.token;
      done();
    });
  });

  it("should get user by token", function(done) {
    request({
      url: 'http://localhost:3000/api/users/authed',
      method: 'GET',
      headers: {
        Authorization: 'Bearer ' + token
      }
    }, function(err, response, body) {
      expect(response.statusCode).to.equal(200);
      userId = JSON.parse(body)._id;
      done();
    });
  });

  it("should signup target user successfully", function(done) {
    request({
      url: url + '/api/authentication/register',
      method: 'POST',
      dataType: 'json',
      json: {
        username: 'targetUser',
        email: 'let@follow.me',
        password: '1234qwer'
      }
    }, function(err, response, body) {
      expect(response.statusCode).to.equal(200);
      done();
    });
  });

  it("should login target user successfully", function(done) {
    request({
      url: url + '/api/authentication/login',
      method: 'POST',
      dataType: 'json',
      json: {
        username: 'targetUser',
        password: '1234qwer'
      }
    }, function(err, response, body) {
      expect(response.statusCode).to.equal(200);
      targetUser = body.user;
      targetUserToken = body.token;
      done();
    });
  });

  it("should target user upload photo to server", function(done) {
    var formData = {
      // Pass data via Streams
      photo: fs.createReadStream(__dirname + '/image.png'),
      status: 'Chuc mung nam moi'
    };
    request({
      url: 'http://localhost:3000/api/photos/upload',
      method: 'POST',
      headers: {
        Authorization: 'Bearer ' + targetUserToken
      },
      formData: formData
    }, function (err, response, body) {
      expect(response.statusCode).to.equal(200);
      photoId = JSON.parse(body).photo_id;
      done();
    });
  });

  it("should follow a user", function(done) {
    request({
      url: url + '/api/follows/' + targetUser._id,
      method: 'POST',
      headers: {
        Authorization: 'Bearer ' + token
      }
    }, function(err, response, body) {
      expect(response.statusCode).to.equal(200)
      expect(JSON.parse(body).target_user_id).to.equal(targetUser._id);
      done();
    });
  });

  it("should can not follow because user does not exists.", function(done) {
    request({
      url: url + '/api/follows/' + 'id tam bay',
      method: 'POST',
      headers: {
        Authorization: 'Bearer ' + token
      }
    }, function(err, response, body) {
      expect(response.statusCode).to.equal(404)
      done();
    });
  });

  it("should get all follow of a user", function(done) {
    request(url + '/api/follows/' + targetUser._id + '/followers', function(err, response, body) {
      expect(response.statusCode).to.equal(200);
      expect(JSON.parse(body).length).to.equal(1);
      done();
    });
  });

  it("should can not get all follow because user does not exists.", function(done) {
    request(url + '/api/follows/' + 'id xam xam', function(err, response, body) {
      expect(response.statusCode).to.equal(404);
      done();
    });
  });

  it("should get 5 user have largest followers (those users are not followed by current user).", function(done) {
    request({
      url: url + '/api/follows/users?filter=largest&limit=5',
      method: 'GET',
      headers: {
        Authorization: 'Bearer ' + token
      }
    }, function(err, response, body) {
      expect(response.statusCode).to.equal(200);
      expect(JSON.parse(body).length).to.equal(1);
      done();
    });
  });

  it("should get all photos of users who current user is following", function(done) {
    request({
      url: url + '/api/follows/photos',
      method: 'GET',
      headers: {
        Authorization: 'Bearer ' + token
      }
    }, function(err, response, body) {
      expect(response.statusCode).to.equal(200);
      expect(JSON.parse(body).length).to.equal(1);
      done();
    });
  });

  it("should get all user a user is following", function(done) {
    request.get(url + '/api/follows/' + userId + '/following', function(err, response, body) {
      expect(response.statusCode).to.equal(200);
      expect(JSON.parse(body).length).to.equal(1);
      done();
    });
  });

  it("should unfollow a user", function(done) {
    request({
      url: url + '/api/follows/' + targetUser._id,
      method: 'DELETE',
      headers: {
        Authorization: 'Bearer ' + token
      }
    }, function(err, response, body) {
      expect(response.statusCode).to.equal(200)
      expect(JSON.parse(body).target_user_id).to.equal(targetUser._id);
      done();
    });
  });

  it("should can not unfollow because user is not exists.", function(done) {
    request({
      url: url + '/api/follows/' + 'id tam bay',
      method: 'DELETE',
      headers: {
        Authorization: 'Bearer ' + token
      }
    }, function(err, response, body) {
      expect(response.statusCode).to.equal(404)
      done();
    });
  });

  after(function() {
    app.stopAndClean();
  });
});
