var expect = require('chai').expect;
var fs = require('fs');
var request = require('request');
var app = require('../server/app');

var url = 'http://localhost:3000';
var token = null;
var photoId = null;
var commentId = null;
var userId = null;

describe('Z_GGWP', function() {
  before(function(done) {
    app.start().then(done).catch(done);
  });

  it("should signup successfully", function(done) {
    request({
      url: url + '/api/authentication/register',
      method: 'POST',
      dataType: 'json',
      json: {
        username: 'jinich',
        email: 'jinich@jinich.jinich',
        password: '1234qwer'
      }
    }, function(err, response, body) {
      expect(response.statusCode).to.equal(200);
      done();
    });
  });

  it("should login successfully", function(done) {
    request({
      url: url + '/api/authentication/login',
      method: 'POST',
      dataType: 'json',
      json: {
        username: 'jinich',
        password: '1234qwer'
      }
    }, function(err, response, body) {
      expect(response.statusCode).to.equal(200);
      token = body.token;
      done();
    });
  });

  it("should upload photo to server", function(done) {
    var formData = {
      // Pass data via Streams
      photo: fs.createReadStream(__dirname + '/image.png'),
      status: 'Chuc mung nam moi'
    };
    request({
      url: 'http://localhost:3000/api/photos/upload',
      method: 'POST',
      headers: {
        Authorization: 'Bearer ' + token
      },
      formData: formData
    }, function (err, response, body) {
      expect(response.statusCode).to.equal(200);
      photoId = JSON.parse(body).photo_id;
      done();
    });
  });

  it('should post a comment', function(done) {
    request({
      url: url + '/api/comments/',
      method: 'POST',
      headers: {
        Authorization: 'Bearer ' + token
      },
      dataType: 'json',
      json: {
        photo_id: photoId,
        comment: 'What a nice photo!!!'
      }
    }, function(err, response, body) {
      expect(response.statusCode).to.equal(200);
      expect(body.photo_id).to.equal(photoId);
      commentId = body._id;
      done();
    });
  });

  it("should get user by token", function(done) {
    request({
      url: 'http://localhost:3000/api/users/authed',
      method: 'GET',
      headers: {
        Authorization: 'Bearer ' + token
      }
    }, function(err, response, body) {
      expect(response.statusCode).to.equal(200);
      userId = JSON.parse(body)._id;
      done();
    });
  });

  it('should find user by username.', function(done) {
    request.get(url + '/api/users?username=jinich', function(err, response, body) {
      expect(response.statusCode).to.equal(200);
      expect(JSON.parse(body).length).to.equal(1);
      done();
    });
  });

  it('should get user by id.', function(done) {
    request.get(url + '/api/users/' + userId + '/info', function(err, response, body) {
      expect(response.statusCode).to.equal(200);
      expect(JSON.parse(body)._id).to.equal(userId);
      done();
    });
  });

  it('should change password of user', function(done) {
    request({
      url: url + '/api/users/password',
      method: 'POST',
      headers: {
        Authorization: 'Bearer ' + token
      },
      dataType: 'json',
      json: {
        old_password: '1234qwer',
        new_password: 'qwer1234'
      }
    }, function(err, response, body) {
      expect(response.statusCode).to.equal(200);
      expect(body.password).to.equal('qwer1234');
      done();
    });
  });

  it('should change avatar and description of user', function(done) {
    var formData = {
      // Pass data via Streams
      avatar: fs.createReadStream(__dirname + '/image.png'),
      description: 'Chuc mung nam moi'
    };
    request({
      url: url + '/api/users/info',
      method: 'POST',
      headers: {
        Authorization: 'Bearer ' + token
      },
      formData: formData
    }, function (err, response, body) {
      expect(response.statusCode).to.equal(200);
      done();
    });
  });

  it('should change avatar and description of user, avatar is not provied', function(done) {
    var formData = {
      description: 'Chuc mung nam moi'
    };
    request({
      url: url + '/api/users/info',
      method: 'POST',
      headers: {
        Authorization: 'Bearer ' + token
      },
      formData: formData
    }, function (err, response, body) {
      expect(response.statusCode).to.equal(200);
      done();
    });
  });

  it('should get all photos uploaded by user', function(done) {
    request(url + '/api/users/' + userId + '/photos', function(err, response, body) {
      expect(response.statusCode).to.equal(200);
      done();
    });
  });

  after(function() {
    app.stopAndClean();
  });
});
