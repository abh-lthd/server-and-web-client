var expect = require('chai').expect;
var request = require('request');
var app = require('../server/app');

var url = 'http://localhost:3000';

describe('Authentication test', function() {
  before(function(done) {
    app.start().then(done).catch(done);
  });

  it("should signup successfully", function(done) {
    request({
      url: url + '/api/authentication/register',
      method: 'POST',
      dataType: 'json',
      json: {
        username: 'jinich',
        email: 'jinich@jinich.jinich',
        password: '1234qwer'
      }
    }, function(err, response, body) {
      expect(response.statusCode).to.equal(200);
      done();
    });
  });

  it("should login successfully", function(done) {
    request({
      url: url + '/api/authentication/login',
      method: 'POST',
      dataType: 'json',
      json: {
        username: 'jinich',
        password: '1234qwer'
      }
    }, function(err, response, body) {
      expect(response.statusCode).to.equal(200);
      done();
    });
  });

  it("should register failed due to username already exists", function(done) {
    request({
      url: url + '/api/authentication/register',
      method: 'POST',
      dataType: 'json',
      json: {
        username: 'jinich',
        email: 'jinich@jinichddd.jinich',
        password: '1234qwer'
      }
    }, function(err, response, body) {
      expect(response.statusCode).to.equal(409);
      done();
    });
  });

  it("should register failed due to email already exists", function(done) {
    request({
      url: url + '/api/authentication/register',
      method: 'POST',
      dataType: 'json',
      json: {
        username: 'ddkdkdk',
        email: 'jinich@jinich.jinich',
        password: '1234qwer'
      }
    }, function(err, response, body) {
      expect(response.statusCode).to.equal(409);
      done();
    });
  });

  it("should register failed due to invalid username", function(done) {
    request({
      url: url + '/api/authentication/register',
      method: 'POST',
      dataType: 'json',
      json: {
        username: '',
        email: 'jinich@jinich.com',
        password: 'dkdkd3838'
      }
    }, function(err, response, body) {
      expect(response.statusCode).to.equal(400);
      expect(body).to.equal('Username is invalid.');
      done();
    });
  });

  it("should register failed due to invalid password", function(done) {
    request({
      url: url + '/api/authentication/register',
      method: 'POST',
      dataType: 'json',
      json: {
        username: 'jinichhhh',
        email: 'jinich@jinich.jinichhhhh',
        password: '1234'
      }
    }, function(err, response, body) {
      expect(response.statusCode).to.equal(400);
      expect(body).to.equal('Password is invalid.');
      done();
    });
  });

  it("should register failed due to invalid email", function(done) {
    request({
      url: url + '/api/authentication/register',
      method: 'POST',
      dataType: 'json',
      json: {
        username: 'hehekaka',
        email: 'invalid.email',
        password: '12345didk'
      }
    }, function(err, response, body) {
      expect(response.statusCode).to.equal(400);
      expect(body).to.equal('Email is invalid.');
      done();
    });
  });

  it("should login failed due to invalid password", function(done) {
    request({
      url: url + '/api/authentication/login',
      method: 'POST',
      dataType: 'json',
      json: {
        username: 'jinich',
        password: '12345',
      }
    }, function(err, response, body) {
      expect(response.statusCode).to.equal(404);
      done();
    });
  });

  it("should login failed due to invalid username", function(done) {
    request({
      url: url + '/api/authentication/login',
      method: 'POST',
      dataType: 'json',
      json: {
        username: 'jinic',
        password: '123456',
      }
    }, function(err, response, body) {
      expect(response.statusCode).to.equal(404);
      done();
    });
  });

  it("should get user by token", function(done) {
    request({
      url: 'http://localhost:3000/api/users/authed',
      method: 'GET',
      headers: {
        Authorization: 'Bearer ' + token
      }
    }, function(err, response, body) {
      expect(response.statusCode).to.equal(200);
      done();
    });
  });

  after(function() {
    app.stopAndClean();
  });
});
