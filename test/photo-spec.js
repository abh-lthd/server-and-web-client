var expect = require('chai').expect;
var request = require('request');
var fs = require('fs');
var app = require('../server/app');

var url = 'http://localhost:3000';
var token = null;
var photoId = null;

describe('Upload photo testing', function() {
  before(function(done) {
    app.start().then(done).catch(done);
  });

  it("should signup successfully", function(done) {
    request({
      url: url + '/api/authentication/register',
      method: 'POST',
      dataType: 'json',
      json: {
        username: 'jinich',
        email: 'jinich@jinich.jinich',
        password: '1234qwer'
      }
    }, function(err, response, body) {
      expect(response.statusCode).to.equal(200);
      done();
    });
  });

  it("should login successfully", function(done) {
    request({
      url: url + '/api/authentication/login',
      method: 'POST',
      dataType: 'json',
      json: {
        username: 'jinich',
        password: '1234qwer'
      }
    }, function(err, response, body) {
      expect(response.statusCode).to.equal(200);
      token = body.token;
      done();
    });
  });

  it("should upload photo to server", function(done) {
    var formData = {
      // Pass data via Streams
      photo: fs.createReadStream(__dirname + '/image.png'),
      status: 'Chuc mung nam moi'
    };
    request({
      url: 'http://localhost:3000/api/photos/upload',
      method: 'POST',
      headers: {
        Authorization: 'Bearer ' + token
      },
      formData: formData
    }, function (err, response, body) {
      expect(response.statusCode).to.equal(200);
      photoId = JSON.parse(body).photo_id;
      done();
    });
  });

  it("should upload failed (connection reset becouse image can not be uploaded to server!!!)", function(done) {
    var formData = {
      // Pass data via Streams
      photo: fs.createReadStream(__dirname + '/image.png'),
      status: 'Chuc mung nam moi'
    };
    request({
      url: 'http://localhost:3000/api/photos/upload',
      method: 'POST',
      headers: {
        Authorization: 'Bearer ' + 'token tam bay'
      },
      formData: formData
    }, function (err, response, body) {
      expect(err).to.not.null;
      done();
    });
  });

  it("should get photo info with right id", function(done) {
    request.get(url + '/api/photos/' + photoId + '/info', function(err, response, body) {
      expect(response.statusCode).to.equal(200);
      expect(JSON.parse(body)._id).to.equal(photoId);
      done();
    });
  });

  it("should get photo image with right id", function(done) {
    request.get(url + '/api/photos/' + photoId, function(err, response, body) {
      expect(response.statusCode).to.equal(200);
      done();
    });
  });

  it("should get all photos uploaded by current user", function(done) {
    request({
      url: url + '/api/photos',
      method: 'GET',
      headers: {
        Authorization: 'Bearer ' + token
      }
    }, function(err, response, body) {
      expect(response.statusCode).to.equal(200);
      expect(JSON.parse(body)).to.be.an('array');
      done();
    });
  });

  it("should update photo status", function(done) {
    request({
      url: url + '/api/photos/' + photoId + '/status',
      method: 'POST',
      headers: {
        Authorization: 'Bearer ' + token
      },
      dateType: 'json',
      json: {
        new_status: 'Happy new year'
      }
    }, function(err, response, body) {
      expect(response.statusCode).to.equal(200);
      expect(body.status).to.equal('Happy new year');
      done();
    });
  });

  it("should delete photo", function(done) {
    request({
      url: url + '/api/photos/' + photoId,
      method: 'DELETE',
      headers: {
        Authorization: 'Bearer ' + token
      }
    }, function(err, response, body) {
      expect(response.statusCode).to.equal(200);
      done();
    });
  });

  after(function() {
    app.stopAndClean();
  });
});
