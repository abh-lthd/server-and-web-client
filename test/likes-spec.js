var expect = require('chai').expect;
var fs = require('fs');
var request = require('request');
var app = require('../server/app');

var url = 'http://localhost:3000';
var token = null;
var photoId = null;
var likeId = null;

describe('Likes testing', function() {
  before(function(done) {
    app.start().then(done).catch(done);
  });

  it("should signup successfully", function(done) {
    request({
      url: url + '/api/authentication/register',
      method: 'POST',
      dataType: 'json',
      json: {
        username: 'jinich',
        email: 'jinich@jinich.jinich',
        password: '1234qwer'
      }
    }, function(err, response, body) {
      expect(response.statusCode).to.equal(200);
      done();
    });
  });

  it("should login successfully", function(done) {
    request({
      url: url + '/api/authentication/login',
      method: 'POST',
      dataType: 'json',
      json: {
        username: 'jinich',
        password: '1234qwer'
      }
    }, function(err, response, body) {
      expect(response.statusCode).to.equal(200);
      token = body.token;
      done();
    });
  });

  it("should upload photo to server", function(done) {
    var formData = {
      // Pass data via Streams
      photo: fs.createReadStream(__dirname + '/image.png'),
      status: 'Chuc mung nam moi'
    };
    request({
      url: 'http://localhost:3000/api/photos/upload',
      method: 'POST',
      headers: {
        Authorization: 'Bearer ' + token
      },
      formData: formData
    }, function (err, response, body) {
      expect(response.statusCode).to.equal(200);
      photoId = JSON.parse(body).photo_id;
      done();
    });
  });

  it("should like a photo", function(done) {
    request({
      url: url + '/api/likes/' + photoId,
      method: 'POST',
      headers: {
        Authorization: 'Bearer ' + token
      }
    }, function(err, response, body) {
      expect(response.statusCode).to.equal(200);
      expect(JSON.parse(body).photo_id).to.equal(photoId);
      done();
    });
  });

  it("should can not like because photo does not exists.", function(done) {
    request({
      url: url + '/api/likes/' + 'photo tam bay',
      method: 'POST',
      headers: {
        Authorization: 'Bearer ' + token
      }
    }, function(err, response, body) {
      expect(response.statusCode).to.equal(404);
      done();
    });
  });

  it("should get all likes of a photo", function(done) {
    request(url + '/api/likes/' + photoId, function(err, response, body) {
      expect(response.statusCode).to.equal(200);
      expect(JSON.parse(body).length).to.equal(1);
      done();
    });
  });

  it("should can not get all likes because photo does not exists", function(done) {
    request(url + '/api/likes/' + 'invalid photo', function(err, response, body) {
      expect(response.statusCode).to.equal(404);
      done();
    });
  });

  it("should unlike a photo", function(done) {
    request({
      url: url + '/api/likes/' + photoId,
      method: 'DELETE',
      headers: {
        Authorization: 'Bearer ' + token
      }
    }, function(err, response, body) {
      expect(response.statusCode).to.equal(200);
      expect(JSON.parse(body).photo_id).to.equal(photoId);
      done();
    });
  });

  it("should can not unlike because photo does not exists.", function(done) {
    request({
      url: url + '/api/likes/' + 'photo tam bay',
      method: 'DELETE',
      headers: {
        Authorization: 'Bearer ' + token
      }
    }, function(err, response, body) {
      expect(response.statusCode).to.equal(404);
      done();
    });
  });

  after(function() {
    app.stopAndClean();
  });
});
